import React, { Component } from 'react';
import logo from '../logo.svg';
import 'bootswatch/dist/superhero/bootstrap.min.css';
import '../main.css'
import { Button } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import Handles from './handles';
import Portrait from './Portrait';
import PlayersList from "./PlayersList";

class App extends Component {

    render() {
        return (
            <div className="container-fluid">
                <div className="App">
                    <Nav activeKey="/home" onSelect={selectedKey => alert(`selected ${selectedKey}`)}>
                        <Nav.Item>
                            <Nav.Link href="/home">Active</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="link-1">Link</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="link-2">Link</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="disabled" disabled>
                                Disabled
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                    <header>
                        <h1> This is a test</h1>
                    </header>
                    <main className="row justify-content-center">
                        <Button>Test </Button>
                        <Portrait name="Woot" />
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                        culpa qui officia deserunt mollit anim id est laborum.
                        {/*<Handles handles={this.state.handles}/>*/}
                        <PlayersList/>
                    </main>
                    <footer>

                    </footer>
                </div>
            </div>
        );
    }
}




export default App;

