import React, {Component} from 'react'
import styled from 'styled-components';

const Avatar = styled.img`
  border-radius: 50%;
  width: ${props => props.size || "50px"};
  height: ${props => props.size || "50px"};
  margin: 0 auto;
  
`;
const Container = styled.div`
    margin: 0 auto;
`;
const Column = styled.div`
    display: inline-block;
    margin: 0 10px;
`;
const Name = styled.div`
    font-size: ${props => props.textSize || "1em"};
`;

class Portrait extends Component {
    render() {
        console.log(this.props.avatar);
        return (
            <Container>
                <Column>
                    <Avatar size={this.props.size} src={this.props.avatar} />
                </Column>
                <Column>
                    <Name textSize = {this.props.textSize}>
                        {this.props.name}
                    </Name>
                </Column>
            </Container>
        )
    }
}
export default Portrait;
