import React from 'react'

const Handles = ({ handles }) => {

    return (
        <div>
            <ul>
            {handles.map(handle =>
                <li key={handle.handle}>{handle.name}</li>
            )}
            </ul>
        </div>
      )
    };
export default Handles